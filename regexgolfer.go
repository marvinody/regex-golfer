package main

import (
	"flag"
	"fmt"
	"regexp"
	"strings"
)


type level struct{
	correct, incorrect string
}

var levels = [...]level{
	{
		"good mood food booz",
		"voez deed node weee",
	},
	{
		"good mood food booz",
		"voez deed node vroo",
	},
}

func main() {
	var level_p = flag.Int("level", 0, "Wanted level to attempt")
	var regexs_p = flag.String("regex", "", "Regex attempting to match level")
	flag.Parse()
	fmt.Println("allowed levels: [ 0,", len(levels)-1,"]")
	fmt.Println("regex:", *regexs_p)
	regex := regexp.MustCompile(*regexs_p)
	level_left_s, level_right_s := levels[*level_p].correct, levels[*level_p].incorrect
	left_sa, right_sa := strings.Split(level_left_s, " "), strings.Split(level_right_s, " ")
	left_ba := checkArrForRegex(regex, left_sa)
	right_ba := checkArrForRegex(regex, right_sa)
	prettyPrint(left_sa, right_sa, left_ba, right_ba)
}

func prettyPrint(left_sa, right_sa []string, left_ba, right_ba []bool) {
	len_left, len_right := len(left_ba), len(right_ba)
	high := len_left
	if len_right > high {
		high = len_right
	}
	// some helper funcs to gen the colors
	correct := func(s string) string {
		return fmt.Sprintf("%s%s%s", FgGreen, s, Reset)
	}
	incorrect := func(s string) string {
		return fmt.Sprintf("%s%s%s", FgRed, s, Reset)
	}
	getString := func(s string, b bool) string {
		if b {
			return correct(s)
		}
		return incorrect(s)
	}
	for idx := 0; idx < high; idx += 1 {
		if idx < len_left {
			fmt.Printf("%s", getString(left_sa[idx], left_ba[idx]))
		}
		fmt.Print("\t")
		if idx < len_right {
			fmt.Printf("%s", getString(right_sa[idx], !right_ba[idx]))
		}
		fmt.Print("\n")
	}

}

func checkArrForRegex(regex *regexp.Regexp, arr []string) []bool {
	retArr := make([]bool, 0, len(arr))
	for _, s := range arr {
		retArr = append(retArr, regex.FindString(s) != "")
	}
	return retArr
}
