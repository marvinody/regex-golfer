package main

import (
  "testing"
  "regexp"
)

func TestCheckArrForRegex(t *testing.T){
  cases := []struct{
    reg_s string
    arr []string
    want []bool
  }{
    {
      "oo",
      []string{"good", "wood", "nope", "noop"},
      []bool{true, true, false, true},
    },
    {
      "[aeiou]",
      []string{"test", "my", "murphy", "nnnnn"},
      []bool{true, false, true, false},
    },
  }
  for _, c := range cases {
    got := checkArrForRegex(regexp.MustCompile(c.reg_s), c.arr)
    for idx, got_b := range got {
      want_b := c.want[idx]
      if got_b != want_b {
        s := c.arr[idx]
        t.Errorf("%s was detected incorrectly with %s: want %t, got %t", s, c.reg_s, want_b, got_b)
      }
    }
  }
}
