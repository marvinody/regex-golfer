#regexgolfer

##Objective:
To make a simple program that one could play regex golf with

##Usage
    # just call it with a level and regex flag (with quotes because of shell)
    regexgolfer -level 0 -regex "o"

##Future
I probably won't mess with this too much later
I thought about adding some sort of memory to it so you don't have to call level all the time but it was more a test of how quickly I could make something silly like this
Unfortunately, within like a half hour of me starting this, I was interrupted for the remainder of the day
So this didn't even get to fulfill the purpose I intended it to. Still, I got it to a working state and that's good enough for now
Other things I would add would be some sort of scoring system and maybe configure the levels to be pulled from another file
